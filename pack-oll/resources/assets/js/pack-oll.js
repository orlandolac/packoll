/*
*
*/
$ = jQuery.noConflict();
$(document).ready(function(){
	if(typeof gravity_form != 'undefined' && gravity_form == 'yes' ){
		$('#toplevel_page_gf_edit_forms').remove();
	}
    	var pluginTr = $('table.wp-list-table tr[data-plugin="pack-oll/pack-oll.php"]');
    	if($(pluginTr).size() > 0){
	    	$(' .plugin-title strong', pluginTr).text(packOllObj.license.name);
	    	$(' .plugin-description p', pluginTr).text('Plugin desenvolvido para o site ' + packOllObj.license.name);
	    	$(' .plugin-version-author-uri', pluginTr).text('Versão 2.0 | Por ' + packOllObj.license.author);
    	}
    if(!packOllUserCaps.administrator){
        $('#menu-dashboard a').attr('href', packOllObj.users.panel);
        
    }
});