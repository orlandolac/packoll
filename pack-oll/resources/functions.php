<?php
	function custom_button_example($wp_admin_bar){
		$_configs = parse_ini_file(dirname(__FILE__) . '/../config.ini', true);

		$msg = (!isset($_COOKIE['superAdmin']))? '+ Mostrar menus avançados' : '- Ocultar menus avançados';
		$href = (!isset($_COOKIE['superAdmin']))? '?'. $_configs['license']['menu_master_var'] : '?'. $_configs['license']['menu_master_var'] . '-';
		$args = [
			'id' => 'pack-oll',
			'title' => $msg,
			'href' => $href,
			'meta' => [
				'class' => 'orlandolacerda-packoll'
			]
		];
		$wp_admin_bar->add_node($args);
	}
	add_action('admin_bar_menu', 'custom_button_example', 100);