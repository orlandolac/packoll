<?php
    /*
    Plugin Name:       PackOll
    Plugin URI:        http://www.orlandolacerda.com
    Description:       Pacote para desenvolvimento ágil em wordpress.
    Version:           2.0.0
    Author:            Orlando Lacerda
    GitHub Plugin URI: https://bitbucket.org/orlandolac/packoll
    GitHub Branch:     master

    Developer: Orlando Lacerda 
    Site: www.orlandolacerda.com
    */
global $packOll;
global $wpdb;
global $acf_local;
if(!class_exists("packOll")){
    class packOll{
        private 
            $dotItemsObject,
            $pathConfigIni,
            $pathResources,
            $sysFilePhp,
            $coreCptTax,
            $configurations;

        private static
            $config;

        public 
            $debug = false,
            $acf = [],
            $assets;

        /**
        * Created an instance of the CustomPlugin class
        */
        public function __construct(){

            #SET VARIABLES PLUGIN
            $this->pathConfigIni = dirname(__FILE__) . '/config.ini';
            $this->pathResources = dirname(__FILE__) . '/resources/';

            #REQUIRE CORE
            require($this->pathResources . 'core-cpt-tax.php');
            $this->coreCptTax = new ollCoreCptTax();

            #SET PROPRIETATES OF SYSTEM
            $this->runIntegrityPlugin();
            $this->configurations = parse_ini_file(dirname(__FILE__) . '/config.ini', true);
            $_configs = $this->configurations;

            #REQUIRE FUNCTIONS
            require($this->pathResources . 'functions.php');

            if($this->debug){
                echo '<pre>';
                    print_r($this->configurations);
                echo '</pre>';
            }

            self::$config = $this->configurations;
            $masterVar = $this->configurations['license']['menu_master_var'];

            $this->forceUrl($this->configurations['license']['forceurl']);
            $this->createOptionPage();
            $this->toJavascript();
            $this->resourcesInclude();
            $this->sysUsers();
            $this->setMasterCookie($masterVar);
            $this->eachPosttype();
            $this->menusTheme();
            $this->removeMenus();

            #DEFINE OF WP
            define('FS_METHOD', 'direct');
            define('WP_HOME', $this->configurations['license']['forceurl']);
            define('WP_SITEURL', $this->configurations['license']['forceurl']);
        }

        /**
        * Check Integrity of the PackOll
        */
        public function runIntegrityPlugin(){
            $this->sysFilePhp = substr($_SERVER['PHP_SELF'], strrpos($_SERVER['PHP_SELF'], '/')+ 1);
            $this->assets = plugins_url('resources/assets', __FILE__);

            //> Check Config Files
            if(!file_exists($this->pathConfigIni)){
                deactivate_plugins(plugin_basename( __FILE__ ));
                $this->message('Impossível ativar. <br/>*O arquivo de configuração não existe!', 'error');
                return false;
            }

            //>THUMBNAIL
            if($this->configurations['license']['thumbnail'])
                add_theme_support( 'post-thumbnails' );

            //> message success
        }

        /**
        * Force Change URL
        */
        public function forceUrl($newUrl){
            if(
                isset($this->configurations['license']['forceurl']) && 
                (strpos($newUrl, 'http://') !== false 
                    || strpos($newUrl, 'https://') !== false)){

                #urls
                $oldUrl = get_site_url();
                if($oldUrl !== $newUrl){
                    if(substr($newUrl,-1) == '/'){
                       $newUrl = substr($newUrl, 0, -1);
                    }
                    global $wpdb;
                    $wpdb->query("UPDATE {$wpdb->prefix}posts SET guid = replace(guid, '{$oldUrl}', '{$newUrl}')");
                    $wpdb->query("UPDATE {$wpdb->prefix}posts SET post_content = replace(post_content, '{$oldUrl}', '{$newUrl}')");
                    $wpdb->query("UPDATE {$wpdb->prefix}postmeta SET meta_value = replace(meta_value, '{$oldUrl}', '{$newUrl}')");
                    $wpdb->query("UPDATE {$wpdb->prefix}options SET option_value = '{$newUrl}' WHERE option_name = 'siteurl'");
                    $wpdb->query("UPDATE {$wpdb->prefix}options SET option_value = '{$newUrl}' WHERE option_name = 'home'");
                }

                do_action('packOll_forceUrl_changed');
            }else{
                do_action('packOll_forceUrl_ok');
            }
        }

        /**
        * INSERT OPTIONS PAGE FOR INSERT FIELD FROM ACF
        */
        public function createOptionPage(){
            $path = dirname(__FILE__) . '/../advanced-custom-fields-pro/';
            if(file_exists( $path . 'acf.php') && class_exists('acf')){

                $this->acfPath = $path;
                do_action('packOll_createOptionPage');

                add_action('init', function(){
                    acf_add_options_page(array(
                        'page_title'    => $this->configurations['license']['name'],
                        'menu_title'    => $this->configurations['license']['menu'],
                        'menu_slug'    =>  'packoll-options',
                        'capability'    => 'edit_posts',
                        'icon_url'      => $this->configurations['license']['menu_icon'],
                        'redirect'      => false
                    ));
                });

                //ACF 
                if(isset($this->configurations['license']['acf_fields']) && count($this->configurations['license']['acf_fields']) > 0){
                    $acfFieldsOptions = $this->configurations['license']['acf_fields'];
                    $name = 'packoll';
                    $label = $this->configurations['license']['menu'];

                    $location = [];
                    $location[] = [
                        'param' => 'options_page',
                        'operator' => '==',
                        'value' => 'packoll-options',
                    ];

                    if(isset($this->configurations['license']['acf_role']) && count($this->configurations['license']['acf_role']) > 0){
                        $acfFieldsRole = $this->configurations['license']['acf_role'];
                        foreach($acfFieldsRole AS $read){
                                if(strrpos($read, '.')){
                                    $dataLocation = $this->dotObject($read, false);
                                    $location[] = [
                                        'param' => $dataLocation[0],
                                        'operator' => $dataLocation[1],
                                        'value' => $dataLocation[2],
                                    ];
                                }else{
                                    $location[] = [
                                        'param' => 'current_user_role',
                                        'operator' => '==',
                                        'value' => $read,
                                    ];
                                }
                            }
                    }

                    $this->generateAcfFields($acfFieldsOptions, $name, $label, $location, false);
                }

            }
        }

        /**
        * SEND OBJECT TO JAVASCRIPT
        */
        public function toJavascript(){
           add_action('init', function(){
                wp_enqueue_script( 'script-pack-oll', plugins_url('resources/assets/js/pack-oll.js', __FILE__), array('jquery'));
                wp_localize_script( "script-pack-oll", "packoll", $this->configurations['license']['name']);
                wp_localize_script( "script-pack-oll", "packOllObj", $this->configurations);
                wp_localize_script( "script-pack-oll", "packOllUserCaps", wp_get_current_user()->allcaps);
            });
        }

        /**
        * SEND OBJECT TO JAVASCRIPT
        */
        public function resourcesInclude(){

            do_action('packOll_resources');
            foreach($this->configurations['resources']['plugin'] AS $plugin){
                 if (!file_exists($this->pathResources . $plugin . '.php')) {
                    deactivate_plugins(plugin_basename( __FILE__ ));
                    packOll_message("Impossível ativar. <br/>*O plugin ( <em>{$plugin}</em> ) não foi encontrado no diretório de recursos do pacote", 'error');
                    return false;
                }
            }

            //> ACTIVE ALL PLUGINS
            foreach($this->configurations['resources']['plugin'] AS $plugin){
                require($this->pathResources . $plugin. '.php');
            }
        }

        /**
        * REMOVE SPECIFIC MENU ITEMS
        */
        public function removeMenus(){
            do_action('packOll_removeMenus');
            $masterVar = $this->configurations['license']['menu_master_var'];
            if(!isset($_GET[$masterVar]) && !isset($_COOKIE[$masterVar])){
                if(isset($this->configurations['adminmenu'])){
                    
                    add_action('admin_menu', function(){
                        $defaultMenu = [
                            'dashboard' => 'index.php',
                            'jetpack' => 'jetpack',
                            'posts' => 'edit.php',
                            'media' => 'upload.php',
                            'jetpack' => 'index.php',
                            'pages' => 'edit.php?post_type=page',
                            'comments' => 'edit-comments.php',
                            'appearance' => 'themes.php',
                            'plugins' => 'plugins.php',
                            'users' => 'users.php',
                            'tools' => 'tools.php',
                            'settings' => 'options-general.php'
                        ];

                        foreach($this->configurations['adminmenu']['remove'] AS $menu){
                            $menu = strtolower($menu);
                            if(isset($defaultMenu[$menu])){
                                remove_menu_page($defaultMenu[$menu]);
                            }else{
                                remove_menu_page($menu);
                            }
                        }
                    });
                }
            }
        }

        /**
        * REMOVE SPECIFIC MENU ITEMS
        */
        public function sysUsers(){
            $users = $this->configurations['users'];

            $allCustomRoles = [];
            foreach($users['role'] AS $role){
                $getRole = $this->dotObject($role);
                array_push($allCustomRoles, $getRole->nameObj);
                
                $objName = 'oll-' . $getRole->nameObj;

                $wpRole = get_role($objName);
                if(!$wpRole && $users['use']){
                    
                    $capabilities = [];
                    $capabilities['read'] = true;
                    foreach($getRole->items AS $cap){
                        $capabilities[$cap] = true;
                    }
                    
                    add_role($objName, $getRole->name, $capabilities);
                    
                }else if(!$users['use']){
                    remove_role($objName);
                }
            }
        }

        /**
        * SHOW ALL MENUS HIDDEN
        */
        public function setMasterCookie($masterVar){
            if(isset($_GET[$masterVar]) && !isset($_COOKIE[$masterVar])){
                setcookie($masterVar, 'active', time() + (60 * 30));
            }

            if(isset($_GET[$masterVar . '-'] ) && !isset($_COOKIE[$masterVar . '-'])){
                setcookie($masterVar, 'inactive', time() - (60*60));
            }
        }

        /**
        * WHILE TO CREATE CUSTOM POST TYPE
        */
        public function eachPosttype(){
            foreach($this->configurations AS $key => $read){
                if(strpos($key, 'posttype:') !== false){
                    $name = strtolower($this->coreCptTax->clearChar(str_replace('posttype:','', $key)));
                    $label = null;
                    $icon = null;
                    $taxonomies = Array();
                    $supports = Array('title', 'revisions', 'editor');
                    foreach($read AS $key => $item)
                        $$key = $item;

                    #posttype
                    add_action('init', function() use($name, $label, $supports, $icon){
                        $this->coreCptTax->cpt($name, $label, 'page', $name, true, $supports, $icon);
                    });

                    #taxonomies
                    foreach($taxonomies AS $tax){
                        
                        $specialData = $this->dotObject($tax);
                        $tax = $specialData->name;
                        $terms = $specialData->items;

                        // $tax_name = $name . '-' . strtolower($this->coreCptTax->clearChar($tax, false));
                        $tax_name = strtolower($this->coreCptTax->clearChar($tax, false));
                        add_action('init', function() use($tax_name, $tax, $name){
                            $this->coreCptTax->tax($tax_name, $tax, $tax, $name, 1, $this->coreCptTax->clearChar($tax, false), 1);
                        });


                        #TERMS
                        if(isset($terms) && count($terms) > 0){
                            foreach($terms AS $term){
                                add_action('init', function() use($term, $tax_name){
                                    wp_insert_term($term, $tax_name);
                                });
                            }
                        }
                    }

                    #ACF GROUPS
                    if(isset($acf_fields) && count($acf_fields) > 0){
                        $label = $label . ' - Informações Extra';
                        $location = [];
                        $location[] = [
                            'param' => 'post_type',
                            'operator' => '==',
                            'value' => $name,
                        ];

                        if(isset($acf_role) && count($acf_role) > 0){
                            foreach($acf_role AS $read){
                                if(strrpos($read, '.')){
                                    $dataLocation = $this->dotObject($read, false)->items;
                                    $location[] = [
                                        'param' => $dataLocation[1],
                                        'operator' => $dataLocation[2],
                                        'value' => $dataLocation[3],
                                    ];
                                }else{
                                    $location[] = [
                                        'param' => 'current_user_role',
                                        'operator' => '==',
                                        'value' => $read,
                                    ];
                                }
                            }
                        }

                        $this->generateAcfFields($acf_fields, $name, $label, $location, false);
                    }
                }
            }
        }


        /**
        * INSERT ACF FIELDS
        */


        /**
        * EXTRACT VALUES ON 
        */
        public function generateAcfFields($acf_fields, $name, $label, $location = [], $debug = false){
            $path = dirname(__FILE__) . '/../advanced-custom-fields-pro/acf.php';
            if(file_exists($path) && class_exists('acf') && function_exists('acf_add_local_field_group')){
                $fields = [];
                foreach($acf_fields AS $read){
                    $required = (strpos($read, '*') > 0)? 1 : 0;
                    $dataFields = $this->dotObject(str_replace('*', '', $read), false);
                    $this->dotItemsObject = $dataFields->items;
                    $key = $dataFields->items[1];
                    $type = end($dataFields->items);

                    #remove defaul objects
                    unset($dataFields->items[1]);
                    unset($dataFields->items[count($dataFields->items)]);

                    $tempField = [];

                    #TRATAMENTS SPECIFICS
                    switch($type){
                        case 'wysiwyg':
                            $tempField['media_upload'] = (in_array('media', $dataFields->items))? 1: 0;
                            break;
                    }

                    $tempField['key'] = $this->coreCptTax->clearChar(strtolower("field_{$name}_{$key}"));
                    $tempField['label'] = $dataFields->name;
                    $tempField['name'] = $this->coreCptTax->clearChar(strtolower($key));
                    $tempField['type'] = $type;
                    $tempField['required'] = $required;

                    #INSERT EXTRA FIELDS
                    foreach($dataFields->items AS $read){
                        $pos = strpos($read, '@');
                        $read = substr($read, 0, $pos);

                        #
                        if(!empty($read)){
                            $tempField[$read] = $this->dotItemValue($read);
                        }
                    }

                    $fields[] = $tempField;
                }

                ##DEBUG
                if($debug){
                    echo "<pre>===== LOCATIONS<br/>";
                    print_r($location);
                    echo "=======FIM========";
                    echo "<br/><br/>===== FIELDS<br/>";
                    print_r($fields);
                    echo "</pre>";
                    exit;
                }

                //Reender to ACF
                //LOCATION PARAM
                // array (
                //     'param' => 'post_type',
                //     'operator' => '==',
                //     'value' => $name,
                // ),
                acf_add_local_field_group(array (
                    'key' => "group_oll_{$name}",
                    'title' => $label,
                    'fields' => $fields,
                    'location' => [ $location ],
                    'menu_order' => 0,
                    'position' => 'normal',
                    'style' => 'default',
                    'label_placement' => 'top',
                    'instruction_placement' => 'label',
                    'hide_on_screen' => '',
                ));
            }
        }

        /**
        * EXTRACT VALUE ON ITEM INSIDE dotObject
        */
        public function dotItemValue($item){
            $itemKey = null;
            foreach($this->dotItemsObject AS $key => $read){
                $pos = strpos($read, '@');
                $readItem = substr($read, 0, $pos);
                if($pos > 0 && $readItem == $item){
                    $read = substr($read, $pos+1);
                    return $read;
                }
            }
        }


        /**
        * CREATE MENU TO THEMES
        */
        public function menusTheme(){
            $menus = $this->configurations['menutheme'];
            if(isset($menus['item']) && count($menus['item']) > 0){
                foreach($menus['item'] AS $read)
                    register_nav_menu(strtolower($this->coreCptTax->clearChar($read, false)), $read);
            }
        }


        /**
        * CONVERT DOT DATA TO OBJECT "[item].sub-item.sub-item" 
        */
        private function dotObject($data, $sort = true){
            $terms = null;
            $result = new stdClass;
            $pos = strpos($data, ']');
            if($pos !== false && strpos($data, '.')){
                $result->items = explode('.', substr($data, $pos + 1));
                
                if(count($result->items) > 0){
                    unset($result->items[0]);
                    if($sort){
                        sort($result->items);
                    }
                }
            }

            if($pos !== false){
                $result->name = substr($data, 1, $pos - 1);
                $result->nameObj = strtolower($this->coreCptTax->clearChar(str_replace(' ', '-', $result->name), false));
            }
            
            return $result;
        }

        /**
        * MESSAGE CLASS
        */
        private function message($text, $type = 'updated') {
            #echo "<div class='{$type}'><h3 style='margin-bottom: 0px;'>packOll</h3> <p>{$text}</p></div>";
        }

        //////////////////////////////////////
        /// WORDPRESS HOOKS
        /////////////////////////////////////
        //RUN ON ACTIVATE PLUGIN
        public static function activate(){
            $name = self::$config['license']['name'];
            self::message("Licenciado para: <strong>{$name}</strong>");
        }

        //RUN ON DESACTIVATE
        public static function deactivate(){

        }

    }
}

if(class_exists('packOll')){
    register_activation_hook(__FILE__, array('packOll', 'activate'));
    register_deactivation_hook(__FILE__, array('packOll', 'deactivate'));
    $packOll = new packOll();
}